package detectionPanneau_Video;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.Features2d;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.xfeatures2d.SURF;

public class Utils {

	private static List<Cercle> cercleTrouve = new ArrayList<Cercle>();
	private static List<Integer> res = new ArrayList<Integer>();


	public static Mat LectureImage(String file) {
		File f = new File(file);
		Mat  m = Imgcodecs.imread(f.getAbsolutePath());
		return m;
	}

	public static void ImShow(String title, Mat img) {
		MatOfByte matOfByte = new MatOfByte();
		Imgcodecs.imencode(".png", img, matOfByte);
		byte[] byteArray = matOfByte.toArray();
		BufferedImage bufImage = null;
		try {
			InputStream in = new ByteArrayInputStream(byteArray);
			bufImage = ImageIO.read(in);
			JFrame frame = new JFrame();
			frame.setTitle(title);
			frame.getContentPane().add(new JLabel(new ImageIcon(bufImage)));
			frame.pack();
			frame.setVisible(true);
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Mat HSV(Mat img) {
		Mat hsv = new Mat(img.size(),img.type());
		Imgproc.cvtColor(img, hsv, Imgproc.COLOR_BGR2HSV);
		return(hsv);
	}

	public static Mat seuilImg(Mat img) {

		Mat hsv = HSV(img);
		Mat threshold_img = new Mat();
		Mat threshold_img1 = new Mat();
		Mat threshold_img2 = new Mat();

		Core.inRange(hsv, new Scalar(0, 80, 80), new Scalar(10, 255, 255),threshold_img1);
		Core.inRange(hsv, new Scalar(150, 80, 80), new Scalar(255, 255, 255),threshold_img2);

		Core.bitwise_or(threshold_img1, threshold_img2, threshold_img);

		Imgproc.GaussianBlur(threshold_img, threshold_img, new Size(9, 9), 2, 2);
		return(threshold_img);
	}

	public static List<MatOfPoint> detectionCercle(Mat imgOri) {

		Mat m = seuilImg(imgOri);
		Mat circleMat = new Mat();
		
		List<MatOfPoint> contours = detectionContour(m);

		Imgproc.HoughCircles(m, circleMat, Imgproc.CV_HOUGH_GRADIENT, 2, 100, 100, 90, 0, 1000); 
		// Methode de Hough sur l'image seuillée en B&W
		

		for (int x = 0; x < circleMat.cols(); x++) {
			Imgproc.drawContours(m, contours, x, new Scalar(255,255,0));
		}

		return(contours);

	}

	public static List<MatOfPoint> detectionContour(Mat threshold_img) {

		int thresh = 100;
		Mat canny_output = new Mat();
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		MatOfInt4 hierarchy = new MatOfInt4();
		Imgproc.Canny(threshold_img, canny_output, thresh, thresh*2);
		Imgproc.findContours(canny_output, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

		Mat drawing = Mat.zeros(canny_output.size(), CvType.CV_8UC3);
		Random rand = new Random();

		for(int i=0;i<contours.size();i++) {
			Scalar color = new Scalar (rand.nextInt(255 - 0 + 1),rand.nextInt(255 - 0 + 1),rand.nextInt(255 - 0 + 1));
			Imgproc.drawContours(drawing, contours, i, color, 1, 8, hierarchy, 0, new Point());
			
		}

		//ImShow("Contours",drawing);
		return(contours);
	}

	public static int tracerCercleTrouve(Mat imgOri, List<MatOfPoint> contours) {

		MatOfPoint2f matOfPoint2f = new MatOfPoint2f();
		float[] radius = new float[1];
		Point center = new Point();
		
		int numCercleTrace=0;
		
		for(int c=0; c<contours.size();c++) {
			MatOfPoint contour = contours.get(c);
			double contourArea = Imgproc.contourArea(contour);
			matOfPoint2f.fromList(contour.toList());
			Imgproc.minEnclosingCircle(matOfPoint2f, center, radius);

			if((contourArea/(Math.PI*radius[0]*radius[0])) >=0.7 && radius[0]>15) {

				Point center2 = new Point(center.x,center.y);
				//Imgproc.circle(imgOri, center, (int)radius[0], new Scalar(0,255,0), 2, 8, 0 );
				Imgproc.rectangle(imgOri, new Point(center.x-(int)radius[0],center.y-(int)radius[0]),new Point(center.x+(int)radius[0], center.y+(int)radius[0]),new Scalar(0,255,0), 1);

				cercleTrouve.add(new Cercle(center2,(int)radius[0]));
				
				numCercleTrace++;
				
			}		
		}
		return(numCercleTrace);
	}

	public static Mat extracCercle(Mat src, Point center, int radius) {

		Mat mask = new Mat(src.rows(), src.cols(), CvType.CV_8U, Scalar.all(0));
		Imgproc.circle(mask, center, radius, new Scalar(255,255,255), -1, 8, 0 );

		Mat masked = new Mat();
		src.copyTo( masked, mask );

		Mat thresh = new Mat();
		Imgproc.threshold( mask, thresh, 1, 255, Imgproc.THRESH_BINARY );

		List<MatOfPoint> contours = new ArrayList<>();
		Imgproc.findContours(thresh, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

		Rect rect = Imgproc.boundingRect(contours.get(0));
		Mat cropped = masked.submat(rect);

		return cropped;

	}

	public static MatOfDMatch matchedPanneau(Mat ref, Mat img) {

		//Mat resizedImg = new Mat();
		//Imgproc.resize(img, resizedImg, ref.size());
		
		Mat resizedImg = new Mat();
		Size sz = new Size(154,154);
		Imgproc.resize(ref, ref, sz);
		//resize the image
		int interpolation = Imgproc.INTER_CUBIC;
		Imgproc.resize(img, resizedImg, sz,0,0,interpolation);


		Mat imgGray = new Mat(resizedImg.rows(), resizedImg.cols(), resizedImg.type());
		Imgproc.cvtColor(resizedImg, imgGray, Imgproc.COLOR_BGR2GRAY);
		Core.normalize(imgGray, imgGray, 0, 255, Core.NORM_MINMAX);

		Mat refGray = new Mat(ref.rows(), ref.cols(), ref.type());
		Imgproc.cvtColor(ref, refGray, Imgproc.COLOR_BGR2GRAY);
		Core.normalize(refGray, refGray, 0, 255, Core.NORM_MINMAX);


		double hessianThreshold = 900;
		int nOctaves = 4, nOctaveLayers = 3;
		boolean extended = false, upright = false;
		SURF detector = SURF.create(hessianThreshold, nOctaves, nOctaveLayers, extended, upright);		

		MatOfKeyPoint keypoints1 = new MatOfKeyPoint(), keypoints2 = new MatOfKeyPoint();
		Mat descriptors1 = new Mat(), descriptors2 = new Mat();
		detector.detectAndCompute(refGray, new Mat(), keypoints1, descriptors1);
		detector.detectAndCompute(imgGray, new Mat(), keypoints2, descriptors2);
		

		DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE);
		List<MatOfDMatch> knnMatches = new ArrayList<MatOfDMatch>();
		matcher.knnMatch(descriptors1, descriptors2, knnMatches, 2);

		//-- Recherche des meilleures matchs 

		float ratioThresh = 0.8f;
		List<DMatch> listOfGoodMatches = new ArrayList<>();
		for (int i = 0; i < knnMatches.size(); i++) {
			if (knnMatches.get(i).rows() > 1) {
				DMatch[] matches = knnMatches.get(i).toArray();
				if (matches[0].distance < ratioThresh * matches[1].distance) {
					listOfGoodMatches.add(matches[0]);
				}
			}
		}

		MatOfDMatch goodMatches = new MatOfDMatch();
		goodMatches.fromList(listOfGoodMatches);

		//-- Draw matches
		Mat imgMatches = new Mat();
		Features2d.drawMatches(ref, keypoints1, resizedImg, keypoints2, goodMatches, imgMatches);

		//ImShow("",imgMatches);

		return(goodMatches);

	}

	public static List<Integer> matchingPanneau(Mat cropped) {

		List<DMatch> matchsImage;
		int[] vitesse = {30,50,70,90,110};
		int[] nbMatchVitesse = new int[vitesse.length+1];
		int indiceMatchVitesse=-1;
		int maxMatchVitesse=-1;



		for(int i=0;i<vitesse.length+1;i++) {	

			if(i<vitesse.length) {
				matchsImage=matchedPanneau(Utils.LectureImage("Ressources/ref"+vitesse[i]+".jpg"), cropped).toList();
				nbMatchVitesse[i]=matchsImage.size();
				//System.out.println(vitesse[i]+" : "+nbMatchVitesse[i]);
			}
			else {
				matchsImage=matchedPanneau(Utils.LectureImage("Ressources/refdouble.jpg"), cropped).toList();
				nbMatchVitesse[i]=matchsImage.size();
				//System.out.println("Double"+" : "+nbMatchVitesse[i]);

			}

			if(nbMatchVitesse[i]>maxMatchVitesse) {
				indiceMatchVitesse=i;
				maxMatchVitesse=nbMatchVitesse[i];
			}	

		}
		res.add(indiceMatchVitesse);

		return(res);
	}

	public static List<Integer> detectionPanneau(Mat img) {

		res = new ArrayList<Integer>();
		cercleTrouve = new ArrayList<Cercle>();

		List<MatOfPoint> panneaux = Utils.detectionCercle(img);

		tracerCercleTrouve(img,panneaux);


		for(int x=0;x<cercleTrouve.size();x++) {
			Mat cropped=extracCercle(img,cercleTrouve.get(x).centre,cercleTrouve.get(x).rayon);
			res  = matchingPanneau(cropped);
		}

		return(res);
	}

	public static boolean tracerCercleDynamique(Mat img) {
		
		cercleTrouve = new ArrayList<Cercle>();
		int num=0;
		boolean nouveauCercle = false;

		List<MatOfPoint> panneaux = Utils.detectionCercle(img);

		num=tracerCercleTrouve(img,panneaux);
		
		if(num<1) {
			nouveauCercle=true;
		}
		
		return(nouveauCercle);
		
	}

}
