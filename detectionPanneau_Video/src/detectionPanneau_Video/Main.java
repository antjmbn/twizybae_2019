package detectionPanneau_Video;

import java.awt.Image;
import java.awt.image.BufferedImage;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;


import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import org.opencv.videoio.VideoCapture;


public class Main {

	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}

	public static interfaceGraphique fenetre;
	public static boolean actionCliquee=false;
	public static boolean resetDone=false;
	public static boolean repaintDone=false;

	public static void main(String[] args) {
		

		fenetre = new interfaceGraphique();
		fenetre.setVisible(true);
		
		lectureVideo(1);

		while(true) {

			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(actionCliquee) {
				
				Main.lectureVideo(fenetre.comboBox.getSelectedIndex()+1);
				
			}
		}
	}
	
	public static void reset() {
		
		fenetre.rebuild();
			
	}
	
	public static void lectureVideo(int numVideo) {
		
		fenetre.btnRelancer.setEnabled(false);
		
		Mat frame = new Mat();
		VideoCapture camera = new VideoCapture("Ressources/video"+numVideo+".avi");  
		List<Integer> PanneauAAnalyser = new ArrayList<Integer>();
		boolean nouveauPanneau = true;

		while (camera.read(frame)) {

			if(PanneauAAnalyser.isEmpty() || nouveauPanneau) {
				PanneauAAnalyser = Utils.detectionPanneau(frame);
				panneauAffichage(PanneauAAnalyser);
				nouveauPanneau = false;
			}
			else {
				nouveauPanneau=Utils.tracerCercleDynamique(frame);
			}

			ImageIcon image = new ImageIcon(Mat2bufferedImage(frame));
			fenetre.vidpanel.setIcon(image);

		}
		
		actionCliquee=false;
		fenetre.btnRelancer.setEnabled(true);
		
	}

	public static BufferedImage Mat2bufferedImage(Mat image) {
		MatOfByte bytemat = new MatOfByte();
		Imgcodecs.imencode(".png", image, bytemat);
		byte[] bytes = bytemat.toArray();
		InputStream in = new ByteArrayInputStream(bytes);
		BufferedImage img = null;
		try {
			img = ImageIO.read(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img;
	}

	public static void panneauAffichage(List<Integer> id) {
		BufferedImage img;
		Image dimg;
		ImageIcon imagePanneau;

		for(int i=0;i<id.size();i++) {

			switch(id.get(i)) {
			case 0:

				img = Mat2bufferedImage(Utils.LectureImage("Ressources/ref30.jpg"));
				dimg = img.getScaledInstance(fenetre.vitessePanel.getWidth(), fenetre.vitessePanel.getHeight(), Image.SCALE_SMOOTH);
				imagePanneau = new ImageIcon(dimg);
				fenetre.vitessePanel.setIcon(imagePanneau);

				break;

			case 1:

				img = Mat2bufferedImage(Utils.LectureImage("Ressources/ref50.jpg"));
				dimg = img.getScaledInstance(fenetre.vitessePanel.getWidth(), fenetre.vitessePanel.getHeight(), Image.SCALE_SMOOTH);
				imagePanneau = new ImageIcon(dimg);
				fenetre.vitessePanel.setIcon(imagePanneau);

				break;

			case 2:

				img = Mat2bufferedImage(Utils.LectureImage("Ressources/ref70.jpg"));
				dimg = img.getScaledInstance(fenetre.vitessePanel.getWidth(), fenetre.vitessePanel.getHeight(), Image.SCALE_SMOOTH);
				imagePanneau = new ImageIcon(dimg);
				fenetre.vitessePanel.setIcon(imagePanneau);

				break;

			case 3:

				img = Mat2bufferedImage(Utils.LectureImage("Ressources/ref90.jpg"));
				dimg = img.getScaledInstance(fenetre.vitessePanel.getWidth(), fenetre.vitessePanel.getHeight(), Image.SCALE_SMOOTH);
				imagePanneau = new ImageIcon(dimg);
				fenetre.vitessePanel.setIcon(imagePanneau);

				break;

			case 4:

				img = Mat2bufferedImage(Utils.LectureImage("Ressources/ref110.jpg"));
				dimg = img.getScaledInstance(fenetre.vitessePanel.getWidth(), fenetre.vitessePanel.getHeight(), Image.SCALE_SMOOTH);
				imagePanneau = new ImageIcon(dimg);
				fenetre.vitessePanel.setIcon(imagePanneau);

				break;

			case 5:

				img = Mat2bufferedImage(Utils.LectureImage("Ressources/refdouble.jpg"));
				dimg = img.getScaledInstance(fenetre.vitessePanel.getWidth(), fenetre.vitessePanel.getHeight(), Image.SCALE_SMOOTH);
				imagePanneau = new ImageIcon(dimg);
				fenetre.vitessePanel.setIcon(imagePanneau);

				break;

			default:
				//System.out.println("Aucun panneau detecté");
				break;

			}

		}	

	}


}




