package detectionPanneau_Video;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class interfaceGraphique extends JFrame {

	public JPanel contentPane;
	
	public JButton btnRelancer;
	
	public JLabel vidpanel;
	public JLabel vitessePanel;
	public static JComboBox<String> comboBox;
	
	private final Action relancerAct = new relancerAction();
	
	public interfaceGraphique(){
		super();
		setLayout(null);
		build(); //On construit notre fenetre

	}

	public void build(){

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		setTitle("Détection de panneau"); //On donne un nom à la fenêtre
		setSize(720,480 + 200); //On donne une taille à notre fenêtre
		setLocationRelativeTo(null); //On centre la fenêtre sur l'écran
		//setResizable(false); //On interdit la redimensionnement de la fenêtre
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //On dit à l'application de se fermer lors du clic sur la croix

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0 , 0));

		contentPane.setBackground(Color.white);

		setContentPane(contentPane);
		contentPane.setLayout(null);
			
		vidpanel = new JLabel();
		vidpanel.setBounds(0, 0, 720, 480);
		vidpanel.setBackground(Color.white);
		contentPane.add(vidpanel);
		
		vitessePanel = new JLabel();
		vitessePanel.setBounds(720/2-64,480+20 ,127, 127);
		contentPane.add(vitessePanel);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(15, 500, 130, 30);

		for(int i=1;i<3;i++) {
			comboBox.addItem("Video n°"+i);
		}
		contentPane.add(comboBox);
		
		btnRelancer = new JButton("Relancer vidéo"); 
		btnRelancer.setBounds(140,500, 150, 30);
		btnRelancer.setAction(relancerAct);
		contentPane.add(btnRelancer);
		
	}
	
	public void rebuild() {
		
		contentPane.remove(vidpanel);
		
		vidpanel = new JLabel();
		vidpanel.setBounds(0, 0, 720, 480);
		vidpanel.setBackground(Color.white);
		contentPane.add(vidpanel);
		
		contentPane.paintImmediately(0, 0, 720, 480);
		
		contentPane.remove(vitessePanel);
		
		vitessePanel = new JLabel();
		vitessePanel.setBounds(720/2-64,480+20 ,127, 127);
		contentPane.add(vitessePanel);
		
		contentPane.paintImmediately(720/2-64,480+20 ,127, 127);
		
		Main.resetDone=true;
		
	}
	
	static class relancerAction extends AbstractAction {
		public relancerAction() {
			putValue(NAME, "Lancer la vidéo");
			putValue(SHORT_DESCRIPTION, "Lancer la vidéo à analyser");
		}
		public void actionPerformed(ActionEvent e) {
			Main.actionCliquee=true;
			
			try {
				Main.reset();
			} catch (Exception exc) {
				// TODO Auto-generated catch block
				exc.printStackTrace();
			
			}
			
			
		}
	}
	
}
