package detectionPanneau_Video;

import org.opencv.core.Point;

public class Cercle {
	
	
	public Point centre;
	public int rayon;
	
	public Cercle() {
		this.centre=new Point();
		this.rayon=0;
	}
	
	public Cercle(Point c, int r) {
		this.centre=c;
		this.rayon=r;
	}

	@Override
	public String toString() {
		return "Cercle [centre=" + centre + ", rayon=" + rayon + "]";
	}
	
	

}
