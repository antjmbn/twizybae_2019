package detectionPanneau_Image;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;



public class main {
	public static interfaceGraphique fenetre;
	public static boolean actionCliquee=true;
	public static int num=1;

	public static void main(String[] args) {
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
					
		Mat imgOrigine = util.LectureImage("photosPanneaux/p"+num+".jpg");
		List<Integer> id = util.detectionPanneau(imgOrigine);
		
		Mat imgOri = util.LectureImage("photosPanneaux/p"+num+".jpg");
		util.tracerCercleTrouve(imgOri, util.detectionContour(imgOrigine));
		fenetre = new interfaceGraphique(imgOri,id);
		
		fenetre.setVisible(true);
		
		while(true) {
			if(actionCliquee) {
				
			}
		}		
			
	}
	
	public static void test() {
		num=fenetre.comboBox.getSelectedIndex()+1;
		
		Mat imgOrigine = util.LectureImage("photosPanneaux/p"+num+".jpg");
		List<Integer> id = util.detectionPanneau(imgOrigine);
		
		Mat imgOri = util.LectureImage("photosPanneaux/p"+num+".jpg");
		util.tracerCercleTrouve(imgOri, util.detectionContour(imgOrigine));
		
		fenetre.rebuild(imgOri, id);
		
		actionCliquee=false;
	}
	
	public static void hsv() {
		num=fenetre.comboBox.getSelectedIndex()+1;
		
		Mat imgOrigine = util.LectureImage("photosPanneaux/p"+num+".jpg");
		
		Mat imgHSV = util.HSV(imgOrigine);
		
		List<Integer> id = new ArrayList<Integer>();
		id.add(-1);
		
		fenetre.rebuild(imgHSV, id);
		
		actionCliquee=false;
	}

}
