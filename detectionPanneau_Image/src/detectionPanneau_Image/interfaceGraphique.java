package detectionPanneau_Image;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import org.opencv.core.Mat;

public class interfaceGraphique extends JFrame {

	private JPanel contentPane;
	private JPanel contentPaneImgReference;
	private JPanel contentPaneImgPanneau;
	public JComboBox<String> comboBox = new JComboBox();
	private ShowImage panelRef = new ShowImage();
	private ShowImage panel30= new ShowImage();
	private ShowImage panel50= new ShowImage();
	private ShowImage panel70= new ShowImage();
	private ShowImage panel90= new ShowImage();
	private ShowImage panel110= new ShowImage();
	private ShowImage panelDouble= new ShowImage();
	
	private List<ShowImage> listePanneauxAffiches = new ArrayList<ShowImage>();
	
	
	private final Action searchAct = new rechercheAction();
	private final Action hsvAct = new hsvAction();
	

	public interfaceGraphique(Mat imgRef,List<Integer> idPanneau){
		super();
		setLayout(null);
		build(imgRef,idPanneau); //On construit notre fenetre

	}

	public void build(Mat imgRef,List<Integer> idPanneau){

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		setTitle("Détection de panneau"); //On donne un nom à la fenêtre
		setSize((int)(screenSize.width/1.25)*2/3 + 10,(int) (screenSize.height/1.25)*2/3 + 200); //On donne une taille à notre fenêtre
		setLocationRelativeTo(null); //On centre la fenêtre sur l'écran
		//setResizable(false); //On interdit la redimensionnement de la fenêtre
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //On dit à l'application de se fermer lors du clic sur la croix

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0 , 0));

		contentPane.setBackground(Color.white);

		setContentPane(contentPane);
		contentPane.setLayout(null);

		comboBox = new JComboBox<String>();
		comboBox.setBounds((int)(this.WIDTH/2+180), (int)(screenSize.height/1.25)*2/3 + 10, 130, 30);

		for(int i=1;i<11;i++) {
			comboBox.addItem("Image n°"+i);
		}

		contentPane.add(comboBox);
		
		JButton btnRecherche = new JButton("Rechercher panneau"); 
		btnRecherche.setBounds((int)(this.WIDTH/2+310), (int)(screenSize.height/1.25)*2/3 + 10, 200, 30);
		btnRecherche.setAction(searchAct);
		contentPane.add(btnRecherche);

		JButton btnHSV = new JButton("hsv"); 
		btnHSV.setBounds((int)(this.WIDTH/2+510), (int)(screenSize.height/1.25)*2/3 + 10, 80, 30);
		btnHSV.setAction(hsvAct);
		contentPane.add(btnHSV);
		
		panelRef = new ShowImage(imgRef);
		panelRef.setBounds(5, 5, (int)(screenSize.width/1.25)*2/3, (int)(screenSize.height/1.25)*2/3);
		panelRef.setBackground(Color.white);
		contentPane.add(panelRef);
		
		listePanneauxAffiches.add(panelRef);

		for(int i=0;i<idPanneau.size();i++) {

			switch(idPanneau.get(i)) {
			case 0:
				panel30 = new ShowImage("basePanneau/ref30.jpg");
				panel30.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panel30);
				
				listePanneauxAffiches.add(panel30);
				
				break;

			case 1:
				panel50 = new ShowImage("basePanneau/ref50.jpg");
				panel50.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panel50);
				
				listePanneauxAffiches.add(panel50);
				
				break;

			case 2:
				panel70 = new ShowImage("basePanneau/ref70.jpg");			
				panel70.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panel70);
				
				listePanneauxAffiches.add(panel70);
				
				break;

			case 3:
				panel90 = new ShowImage("basePanneau/ref90.jpg");
				panel90.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panel90);
				
				listePanneauxAffiches.add(panel90);
				
				break;

			case 4:
				panel110 = new ShowImage("basePanneau/ref110.jpg");
				panel110.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panel110);
				
				listePanneauxAffiches.add(panel110);
				
				break;

			case 5:
				panelDouble = new ShowImage("basePanneau/refdouble.jpg");
				panelDouble.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panelDouble);
				
				listePanneauxAffiches.add(panelDouble);
				
				break;

			default:
				//System.out.println("Aucun panneau detecté");
				listePanneauxAffiches.removeAll(listePanneauxAffiches);
				break;

			}

		}	

	}

	public void rebuild(Mat imgRef,List<Integer> idPanneau) {
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		for(int i=0;i<listePanneauxAffiches.size();i++) {
			contentPane.remove(listePanneauxAffiches.get(i));
		}
		
		listePanneauxAffiches.removeAll(listePanneauxAffiches);
		
		contentPane.revalidate();
		contentPane.repaint();

		panelRef = new ShowImage(imgRef);
		panelRef.setBounds(5, 5, (int)(screenSize.width/1.25)*2/3, (int)(screenSize.height/1.25)*2/3);
		panelRef.setBackground(Color.white);
		contentPane.add(panelRef);
		
		listePanneauxAffiches.add(panelRef);

		for(int i=0;i<idPanneau.size();i++) {

			switch(idPanneau.get(i)) {
			case 0:
				panel30 = new ShowImage("basePanneau/ref30.jpg");
				panel30.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panel30);
				
				listePanneauxAffiches.add(panel30);
				
				break;

			case 1:
				panel50 = new ShowImage("basePanneau/ref50.jpg");
				panel50.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panel50);
				
				listePanneauxAffiches.add(panel50);
				
				break;

			case 2:
				panel70 = new ShowImage("basePanneau/ref70.jpg");
				panel70.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panel70);
				
				listePanneauxAffiches.add(panel70);
				
				break;

			case 3:
				panel90 = new ShowImage("basePanneau/ref90.jpg");
				panel90.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panel90);
				
				listePanneauxAffiches.add(panel90);
				
				break;

			case 4:
				panel110 = new ShowImage("basePanneau/ref110.jpg");
				panel110.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panel110);
				
				listePanneauxAffiches.add(panel110);
				
				break;

			case 5:
				panelDouble = new ShowImage("basePanneau/refdouble.jpg");
				panelDouble.setBounds((int)((screenSize.width/1.25)*2/3)/(idPanneau.size()+1) - 64 + (130*i) , (int)(screenSize.height/1.25*2/3+45), 127, 127);
				contentPane.add(panelDouble);
				
				listePanneauxAffiches.add(panelDouble);
				
				break;

			default:
				//System.out.println("Aucun panneau detecté");
				//listePanneauxAffiches.removeAll(listePanneauxAffiches);
				break;

			}

		}	


	}
	
	private class rechercheAction extends AbstractAction {
		public rechercheAction() {
			putValue(NAME, "Rechercher panneau");
			putValue(SHORT_DESCRIPTION, "Rechercher un panneau sur l'image");
		}
		public void actionPerformed(ActionEvent e) {
			main.actionCliquee=true;
	    	main.test();
		}
	}
	
	private class hsvAction extends AbstractAction {
		public hsvAction() {
			putValue(NAME, "HSV");
			putValue(SHORT_DESCRIPTION, "Convertir l'image en HSV");
		}
		public void actionPerformed(ActionEvent e) {
			main.actionCliquee=true;
	    	main.hsv();
		}
	}
}
