package detectionPanneau_Image;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

public class ShowImage extends JPanel {
	BufferedImage  image;
	
	public ShowImage() {
		image=null;
	}

	public ShowImage(Mat img) {
		MatOfByte matOfByte = new MatOfByte();
		Imgcodecs.imencode(".png", img, matOfByte);
		byte[] byteArray = matOfByte.toArray();
		try {
			InputStream in = new ByteArrayInputStream(byteArray);
			image = ImageIO.read(in);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ShowImage(String filePath) {
		try {
			File input = new File(filePath);
			image = ImageIO.read(input);
		} catch (IOException ie) {
			System.out.println("Error:"+ie.getMessage());
		}
	}
	
	
	public double getScaleFactor(int iMasterSize, int iTargetSize) {

	    double dScale = 1;
	    if (iMasterSize > iTargetSize) {

	        dScale = (double) iTargetSize / (double) iMasterSize;

	    } else {

	        dScale = (double) iTargetSize / (double) iMasterSize;

	    }

	    return dScale;

	}

	public double getScaleFactorToFit(Dimension original, Dimension toFit) {

	    double dScale = 1d;

	    if (original != null && toFit != null) {

	        double dScaleWidth = getScaleFactor(original.width, toFit.width);
	        double dScaleHeight = getScaleFactor(original.height, toFit.height);

	        dScale = Math.min(dScaleHeight, dScaleWidth);

	    }

	    return dScale;

	}

	@Override
	protected void paintComponent(Graphics g) {

	    super.paintComponent(g);

	    double scaleFactor = Math.min(1d, getScaleFactorToFit(new Dimension(image.getWidth(), image.getHeight()), getSize()));

	    int scaleWidth = (int) Math.round(image.getWidth() * scaleFactor);
	    int scaleHeight = (int) Math.round(image.getHeight() * scaleFactor);

	    Image scaled = image.getScaledInstance(scaleWidth, scaleHeight, Image.SCALE_SMOOTH);

	    int width = getWidth() - 1;
	    int height = getHeight() - 1;

	    int x = (width - scaled.getWidth(this)) / 2;
	    int y = (height - scaled.getHeight(this)) / 2;

	    g.drawImage(scaled, x, y, this);

	}

}