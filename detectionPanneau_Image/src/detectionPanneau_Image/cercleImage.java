package detectionPanneau_Image;

import org.opencv.core.Point;

public class cercleImage {
	
	
	public Point centre;
	public int rayon;
	
	public cercleImage() {
		this.centre=new Point();
		this.rayon=0;
	}
	
	public cercleImage(Point c, int r) {
		this.centre=c;
		this.rayon=r;
	}

	@Override
	public String toString() {
		return "cercleImage [centre=" + centre + ", rayon=" + rayon + "]";
	}
	
	

}
