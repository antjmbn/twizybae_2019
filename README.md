# Project Twizy 2019

##### Groupe de Baptiste REVEILLER, Antoine JAMBON, Eloi GABORIT

*Ce projet est un projet �tudiant utilisant JAVA avec la biblioth�que OpenCV*

Il consiste � l'analyse d'image et la d�tection de panneaux de signalisation routi�re de vitesse. 

Il s'organise en 2 parties :

   	� La premi�re partie : la d�tection de panneaux routiers sur une image fixe 
   	� La seconde partie : la d�tection de panneaux routiers sur un flux d'image (vid�o en AVI)

Cette derni�re partie tend � �tre int�gr�e sur un v�hicule pour effectuer un test grandeur nature.

---

## D�tection de panneau sur une image
##### Utilisation de l'interface graphique

L'interface, par d�fault, charge la premi�re image de la base de donn�e et affiche le-s panneau-x pr�sent-s.\n
L'utilisateur peut alors choisir une autre image (10 images sont pr�sentes dans l'exemple) avec le menu d�roulant (exemple "Photo n�1").

Puis ensuite, choisir s'il veut :

   	� transformer l'image de RGB en HSV gr�ce au bouton nomm� "HSV" 
   	� choisir de rechercher les panneaux de signalisations gr�ce au bouton nomm� "Rechercher panneau"
   
Le r�sultat de la recherche de panneau s'affiche alors en dessous des boutons et du menu d�roulant.

---

## D�tection de panneau sur une vid�o
##### Utilisation de l'interface graphique

L'interface, par d�fault, lance la premi�re vid�o et affiche le r�sultat des panneaux trouv�s.

L'utilisateur peut choisir, � la fin d'une vid�o, une autre vid�o (2 vid�o sont pr�sentes dans l'exemple) avec le menu d�roulant (exemple "Video n�1").
Puis ensuite, choisir d'analyser une autre vid�o gr�ce au bouton nomm� "Lancer vid�o". 

	Ce bouton n'est pas utilisable tant que la vid�o n'est pas termin�e.
   
Le r�sultat s'affiche alors en dessous du bouton et du menu d�roulant.

---

## M�thodes utilis�es
##### La m�thode de d�tection des panneaux est commune au deux parties.


M�thode :

	� Tranformation de l'image de RGB en HSV 
	-> Utils.HSV()
	
	� Seuillage de l'image pour obtenir que les tons de rouge/bordeaux (couleurs des panneaux), on obtient une image B&W 
	-> Utils.seuilImg()
	
	� D�tection de contours avec la m�thode de CANNY 
	-> Utils.detectionContour()
	
	� Recherche des cercles avec l'Algorithme de HOUGH 
	-> Utils.detectionCercle()
	
	� Extraction d'une partie de l'image o� est pr�sent le panneau 
	-> Utils.extracCercle()
	
	� Matching en utilisant la m�thode SURF detector de OpenCV sur des panneaux de r�f�rence 
	-> Utils.matchedPanneau()
	

### Base de donn�es - SQL
##### Cr�ation d'une base de donn�es 

Cette base de donn�es permet de stocker les chemins des images des panneaux ainsi que les points d'int�rets d�tect�s 
durant le matching pour utiliser ceux-ci pour entrainer un r�seau neuronal.

	Fichier sql 
	- creationBD //Permet la cr�ation de la base de donn�es et des tableaux de celles-ci
	- ajoutDonnees //Permet l'ajout des donn�es de r�f�rences 
	
