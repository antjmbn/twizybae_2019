 create database Twizy_Projet;
 
 CREATE TABLE imgPanneau (
 idPanneau VARCHAR(20) primary key not null,
 cheminImg varchar(40)
 );
 
 CREATE TABLE pointsInterets (
 idPI int primary key not null auto_increment,
 absX float,
 absY float,
 idPanneau varchar(20)
 );
 
CREATE TABLE descriptionPanneau (
 idPanneau varchar(20) primary key not null,
 type varchar(40) not null,
 forme varchar(20) not null,
 couleurHSV varchar(9)
 ); 
